﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ваше имя?");
            string name = Console.ReadLine();
            Console.WriteLine("Ваш возраст?");
            int age = int.Parse(Console.ReadLine());
            
            string s1 = $"Привет, {name}! Тебе уже {age} лет.";
            string s2 = String.Format("Привет, {0}! Тебе уже {1} лет", name, age);
            Console.WriteLine("Ответ:");
            Console.WriteLine(s1);
            Console.WriteLine(s2);
        }
    }
}
