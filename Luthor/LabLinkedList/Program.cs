﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<string> x = new LinkedList<string>();
            x.AddLast("Труд");
            x.AddFirst("Мир");
            x.AddAfter(x.Last, "Май");
            //
            foreach (string i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
