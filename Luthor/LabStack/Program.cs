﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabStack
{
    class Program
    {
        static void Main(string[] args)
        {
            //Stack<int> x = new Stack<int>();
            Stack x = new Stack();
            x.Push("Рязань");
            x.Push(123);
            x.Push("Тула");
            //x.Push(456);
            //x.Push(789);
            //
            Console.WriteLine(x.Peek());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            #region Luthor
            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка:" + ex.Message);
            }
            #endregion
        }
    }
}
